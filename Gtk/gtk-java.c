#include "gtk-java.h"

static jfieldID gtkobj_fieldID = NULL;

GtkObject *
jobject_to_gtk(JNIEnv *env, jobject obj)
{
    jint obj_pointer;

    if(!gtkobj_fieldID)
	gtkobj_fieldID = (*env)->GetFieldID(env, (*env)->GetObjectClass(env, obj), "obj_pointer", "I");

    obj_pointer = (*env)->GetIntField(env, obj, gtkobj_fieldID);

    return GTK_OBJECT((void *)obj_pointer);
}

void
jobject_set_gtk(JNIEnv *env, jobject obj, GtkObject *gtkobj)
{
    jint obj_pointer;

    obj_pointer = (jint)gtkobj;
    gtk_object_ref(gtkobj);
    gtk_object_sink(gtkobj);

    if(!gtkobj_fieldID)
	gtkobj_fieldID = (*env)->GetFieldID(env, (*env)->GetObjectClass(env, obj), "obj_pointer", "I");

    (*env)->SetIntField(env, obj, gtkobj_fieldID, obj_pointer);
}
