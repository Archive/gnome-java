package Gtk;

import Gtk.Widget;

public class Container extends Gtk.Widget {
    public native void add(Gtk.Widget widget);
    public native void remove(Gtk.Widget widget);
    public native void set_border_width(int border_width);
//    public native void set_resize_mode(Gtk.ResizeMode resize_mode);
};
