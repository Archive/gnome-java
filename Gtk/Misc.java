package Gtk;

import Gtk.Widget;

public abstract class Misc extends Gtk.Widget {

  public native void set_alignment (float xalign, float yalign);
  public native void set_padding (int xpad, int ypad);
};
