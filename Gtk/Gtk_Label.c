#include "Gtk_VBox.h"
#include "gtk-java.h"


/*
 * Class:     Gtk_VBox
 * Method:    construct
 * Signature: (ZI)V
 */
JNIEXPORT void JNICALL Java_Gtk_Label_construct
  (JNIEnv *env, jobject obj, jstring str)
{
	jboolean *copy;

	jobject_set_gtk(env, obj, GTK_OBJECT(gtk_label_new((*env)->GetStringUTFChars(env, str, copy))));
}