package Gtk;

import Gtk.Container;

public class HBox extends Gtk.Box {

	private native void construct(boolean homogeneous, int spacing);

    public HBox(boolean homogeneous, int spacing) {
		construct(homogeneous, spacing);
    }
};
