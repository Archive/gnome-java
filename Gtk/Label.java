package Gtk;

import Gtk.Misc;

public class Label extends Gtk.Misc {

	private native void construct(String str);

    public Label(String str) {
		construct(str);
    }
};
