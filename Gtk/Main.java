package Gtk;

public class Main {
    public static native void init(String appname, String args[]);

    public static native void run();

    public static native int level();

    public static native void quit();

    public static native void iteration();

    static {
	System.loadLibrary("gtk-java");
    };
};
