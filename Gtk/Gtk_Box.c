#include "Gtk_Box.h"
#include "gtk-java.h"


/*
 * Class:     Gtk_Box
 * Method:    gtk_box_pack_start
 * Signature: (LGtk/Widget;ZZI)V
 */
JNIEXPORT void JNICALL Java_Gtk_Box_pack_1start
  (JNIEnv *env, jobject obj, jobject child, jboolean expand, jboolean fill, jint padding)
{
	gtk_box_pack_start(GTK_BOX(jobject_to_gtk(env, obj)),
						GTK_WIDGET(jobject_to_gtk(env, child)), 
						expand, fill, padding);
}
