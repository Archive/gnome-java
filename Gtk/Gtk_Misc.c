#include "Gtk_Misc.h"
#include "gtk-java.h"

/*
 * Class:     Gtk_Misc
 * Method:    set_alignment
 * Signature: (FF)V
 */
JNIEXPORT void JNICALL Java_Gtk_Misc_set_1alignment
  (JNIEnv *env, jobject obj, jfloat xalign, jfloat yalign)
{
	gtk_misc_set_alignment(GTK_MISC(jobject_to_gtk(env, obj)), xalign, yalign);
}

/*
 * Class:     Gtk_Misc
 * Method:    set_padding
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_Gtk_Misc_set_1padding
  (JNIEnv *env, jobject obj, jint xpad, jint ypad)
{
	gtk_misc_set_padding(GTK_MISC(jobject_to_gtk(env, obj)), xpad, ypad);
}