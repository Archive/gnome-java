#include "Gtk_Widget.h"
#include "gtk-java.h"

#define JGTKW(x) GTK_WIDGET(JGTKO(x))

/*
 * Class:     Gtk_Widget
 * Method:    activate
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL
Java_Gtk_Widget_activate(JNIEnv *env, jobject obj)
{
    return gtk_widget_activate(JGTKW(obj));
}

/*
 * Class:     Gtk_Widget
 * Method:    draw_default
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_Gtk_Widget_draw_1default(JNIEnv *env, jobject obj)
{
    gtk_widget_draw_default(JGTKW(obj));
}

/*
 * Class:     Gtk_Widget
 * Method:    draw_focus
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_Gtk_Widget_draw_1focus(JNIEnv *env, jobject obj)
{
    gtk_widget_draw_focus(JGTKW(obj));
}

/*
 * Class:     Gtk_Widget
 * Method:    hide
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_Gtk_Widget_hide(JNIEnv *env, jobject obj)
{
    gtk_widget_hide(JGTKW(obj));
}

/*
 * Class:     Gtk_Widget
 * Method:    hide_all
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_Gtk_Widget_hide_1all(JNIEnv *env, jobject obj)
{
    gtk_widget_hide_all(JGTKW(obj));
}

/*
 * Class:     Gtk_Widget
 * Method:    map
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_Gtk_Widget_map(JNIEnv *env, jobject obj)
{
    gtk_widget_map(JGTKW(obj));
}

/*
 * Class:     Gtk_Widget
 * Method:    popup
 * Signature: (II)V
 */
JNIEXPORT void JNICALL
Java_Gtk_Widget_popup(JNIEnv *env, jobject obj, jint x, jint y)
{
    gtk_widget_popup(JGTKW(obj), x, y);
}

/*
 * Class:     Gtk_Widget
 * Method:    queue_clear
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_Gtk_Widget_queue_1clear(JNIEnv *env, jobject obj)
{
    gtk_widget_queue_clear(JGTKW(obj));
}

/*
 * Class:     Gtk_Widget
 * Method:    queue_clear_area
 * Signature: (IIII)V
 */
JNIEXPORT void JNICALL
Java_Gtk_Widget_queue_1clear_1area(JNIEnv *env, jobject obj, jint x, jint y, jint width, jint height)
{
    gtk_widget_queue_clear_area(JGTKW(obj), x, y, width, height);
}

/*
 * Class:     Gtk_Widget
 * Method:    queue_draw
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_Gtk_Widget_queue_1draw(JNIEnv *env, jobject obj)
{
    gtk_widget_queue_draw(JGTKW(obj));
}

/*
 * Class:     Gtk_Widget
 * Method:    queue_draw_area
 * Signature: (IIII)V
 */
JNIEXPORT void JNICALL
Java_Gtk_Widget_queue_1draw_1area(JNIEnv *env, jobject obj, jint x, jint y, jint width, jint height)
{
    gtk_widget_queue_draw_area(JGTKW(obj), x, y, width, height);
}

/*
 * Class:     Gtk_Widget
 * Method:    queue_resize
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_Gtk_Widget_queue_1resize(JNIEnv *env, jobject obj)
{
    gtk_widget_queue_resize(JGTKW(obj));
}

/*
 * Class:     Gtk_Widget
 * Method:    realize
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_Gtk_Widget_realize(JNIEnv *env, jobject obj)
{
    gtk_widget_realize(JGTKW(obj));
}

/*
 * Class:     Gtk_Widget
 * Method:    reparent
 * Signature: (LGtk/Widget;)V
 */
JNIEXPORT void JNICALL
Java_Gtk_Widget_reparent(JNIEnv *env, jobject obj, jobject new_parent)
{
    gtk_widget_reparent(JGTKW(obj), JGTKW(new_parent));
}

/*
 * Class:     Gtk_Widget
 * Method:    set_sensitive
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_Gtk_Widget_set_1sensitive(JNIEnv *env, jobject obj, jboolean is_sensitive)
{
    gtk_widget_set_sensitive(JGTKW(obj), is_sensitive);
}

/*
 * Class:     Gtk_Widget
 * Method:    set_usize
 * Signature: (II)V
 */
JNIEXPORT void JNICALL
Java_Gtk_Widget_set_1usize(JNIEnv *env, jobject obj, jint x, jint y)
{
    gtk_widget_set_usize(JGTKW(obj), x, y);
}

/*
 * Class:     Gtk_Widget
 * Method:    show
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_Gtk_Widget_show(JNIEnv *env, jobject obj)
{
    gtk_widget_show(JGTKW(obj));
}

/*
 * Class:     Gtk_Widget
 * Method:    show_all
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_Gtk_Widget_show_1all(JNIEnv *env, jobject obj)
{
    gtk_widget_show_all(JGTKW(obj));
}

/*
 * Class:     Gtk_Widget
 * Method:    show_now
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_Gtk_Widget_show_1now(JNIEnv *env, jobject obj)
{
    gtk_widget_show_now(JGTKW(obj));
}

/*
 * Class:     Gtk_Widget
 * Method:    unmap
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_Gtk_Widget_unmap(JNIEnv *env, jobject obj)
{
    gtk_widget_unmap(JGTKW(obj));
}

/*
 * Class:     Gtk_Widget
 * Method:    unparent
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_Gtk_Widget_unparent(JNIEnv *env, jobject obj)
{
    gtk_widget_unparent(JGTKW(obj));
}

/*
 * Class:     Gtk_Widget
 * Method:    unrealize
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_Gtk_Widget_unrealize(JNIEnv *env, jobject obj)
{
    gtk_widget_unrealize(JGTKW(obj));
}
