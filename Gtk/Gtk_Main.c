#include "Gtk_Main.h"
#include <gtk/gtk.h>

static const char *
jstring_to_cstring(JNIEnv *env, jstring str)
{
    return (*env)->GetStringUTFChars(env, str, NULL);
}

/*
 * Class:     Gtk_Main
 * Method:    init
 * Signature: ([Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL
Java_Gtk_Main_init(JNIEnv *env, jclass jc, jstring appname, jobjectArray argv)
{
  int initial_argc, argc = 0, i;
  const char **my_argv = NULL;

  argc = (*env)->GetArrayLength(env, argv);
  argc++; /* We always have appname */
  initial_argc = argc;

  my_argv = alloca(argc * sizeof(char *));

  my_argv[0] = jstring_to_cstring(env, appname);

  for(i = 1; i < argc; i++) {
      jobject obj;

      obj = (*env)->GetObjectArrayElement(env, argv, i - 1);
      my_argv[i] = jstring_to_cstring(env, obj);
  }

  gtk_init(&argc, (char ***)&my_argv);

  (*env)->ReleaseStringUTFChars(env, appname, my_argv[0]);
  for(i = 1; i < initial_argc; i++)
      (*env)->ReleaseStringUTFChars(env, (*env)->GetObjectArrayElement(env, argv, i - 1), my_argv[i]);
}

/*
 * Class:     Gtk_Main
 * Method:    iteration
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_Gtk_Main_iteration(JNIEnv *env, jclass jc)
{
  gtk_main_iteration();
}

/*
 * Class:     Gtk_Main
 * Method:    level
 * Signature: ()I
 */
JNIEXPORT jint JNICALL
Java_Gtk_Main_level(JNIEnv *env, jclass jc)
{
  return (jint)gtk_main_level();
}

/*
 * Class:     Gtk_Main
 * Method:    main
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_Gtk_Main_run(JNIEnv *env, jclass jc)
{
  gtk_main();
}

/*
 * Class:     Gtk_Main
 * Method:    quit
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_Gtk_Main_quit
  (JNIEnv *env, jclass jc)
{
  gtk_main_quit();
}
