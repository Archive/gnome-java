package Gtk;

import Gtk.Object;

public class Widget extends Gtk.Object {
	public native void unparent();
	public native void show();
	public native void hide();
	public native void show_now();
	public native void show_all();
	public native void hide_all();
	public native void map();
	public native void unmap();
	public native void realize();
	public native void unrealize();
	public native void queue_draw();
	public native void queue_draw_area(int x, int y, int width, int height);
	public native void queue_clear();
	public native void queue_clear_area(int x, int y, int width, int height);
	public native void queue_resize();
	public native void draw_focus();
	public native void draw_default();
	public native boolean activate();
	public native void reparent(Gtk.Widget new_parent);
	public native void popup(int x, int y);
	public native void set_sensitive(boolean is_sensitive);
	public native void set_usize(int x, int y);
//	public native void draw(GdkRectangle area);
//	public native void size_request(GtkRequisition requisition);
//	public native void size_allocate(GtkAllocation allocation);
};
