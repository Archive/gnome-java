#include "Gtk_Object.h"
#include "gtk-java.h"

/*
 * Class:     Gtk_Object
 * Method:    get
 * Signature: (Ljava/lang/String;)Ljava/lang/Object;
 */
JNIEXPORT jobject JNICALL
Java_Gtk_Object_get(JNIEnv *env, jobject obj, jstring str)
{
    return NULL;
}

/*
 * Class:     Gtk_Object
 * Method:    ref
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_Gtk_Object_ref(JNIEnv *env, jobject obj)
{
  gtk_object_ref(JGTKO(obj));
  gtk_object_sink(JGTKO(obj));
}

typedef struct {
    JNIEnv *env;
    jobject obj;
    jmethodID methodID;
} jobject_cb_info;

static void
jobject_signal_cb(GtkObject *object, gpointer data,
		  guint n_args, GtkArg *args)
{
    jvalue *jargs;
    jobject_cb_info *cbi = data;
    int i;
    jmethodID mid;

    g_message("signal has %d args", n_args);
    jargs = alloca(n_args * sizeof(jvalue));

    for(i = 0; i < n_args; i++) {
	switch(args[i].type) {
	default:
	    jargs[i].i = GPOINTER_TO_UINT(GTK_VALUE_POINTER(args[i]));
	}
    }

    mid = cbi->methodID;

    switch(args[n_args].type) {
    case GTK_TYPE_BOOL:
	GTK_VALUE_BOOL(args[n_args]) = (* cbi->env)->CallBooleanMethodA(cbi->env, cbi->obj, mid, jargs);
	break;
    default:
	g_error("Return type %d NYI", args[n_args].type);
	break;
    }
}

/*
Type Signature            Java Type 
     Z boolean 
     B byte 
     C char 
     S short 
     I int 
     J long 
     F float 
     D double 
     L fully-qualified-class ;   fully-qualified-class 
     [ type                  type[] 
     ( arg-types ) ret-type     method type 
 */
static void
jobject_arg_signature(GString *str, GtkType argtype)
{
    char *as = NULL, ac = '\0';
    gboolean free_as = FALSE;

    switch(argtype) {
    case GTK_TYPE_NONE:
	ac = 'V';
	break;  
    case GTK_TYPE_CHAR:
    case GTK_TYPE_UCHAR:
	ac = 'C';
	break;
    case GTK_TYPE_BOOL:
	ac = 'Z';
	break;
    case GTK_TYPE_INT:
    case GTK_TYPE_UINT:
    case GTK_TYPE_ENUM:
    case GTK_TYPE_FLAGS:
	ac = 'I';
	break;
    case GTK_TYPE_LONG:
    case GTK_TYPE_ULONG:
	ac = 'J';
	break;
    case GTK_TYPE_FLOAT:
	ac = 'F';
	break;
    case GTK_TYPE_DOUBLE:
	ac = 'D';
	break;
    case GTK_TYPE_STRING:
	as = "Ljava/lang/String;";
	break;
    case GTK_TYPE_BOXED:
    case GTK_TYPE_POINTER:
    case GTK_TYPE_SIGNAL:
    case GTK_TYPE_ARGS:
    case GTK_TYPE_CALLBACK:
    case GTK_TYPE_C_CALLBACK:
    case GTK_TYPE_FOREIGN:
    default:
	ac = 'I'; /* Be dumb, pass the pointer value as an int */
	break;
    case GTK_TYPE_OBJECT:
	as = "LGtk/Object;";
	break;
    case GTK_TYPE_INVALID:
	g_error("Type %d - should not be reached", argtype);
	break;
    }

    if(ac)
	g_string_append_c(str, ac);
    if(as)
	g_string_append(str, as);

    if(free_as)
	g_free(as);
}

static char *
jobject_signal_signature(GtkSignalQuery *siginfo)
{
    GString *tmpstr;
    char *retval;
    int i;

    tmpstr = g_string_new(NULL);

    g_string_append_c(tmpstr, '(');

    for(i = 0; i < siginfo->nparams; i++) {
	GtkType argtype;

	argtype = siginfo->params[i];
	jobject_arg_signature(tmpstr, argtype);
    }

    g_string_append_c(tmpstr, ')');
    jobject_arg_signature(tmpstr, siginfo->return_val);

    retval = tmpstr->str;
    g_string_free(tmpstr, FALSE);

    g_message("Got signature %s for signal %s", retval, siginfo->signal_name);

    return retval;
}

static jobject_cb_info *
jobject_cb_info_new(JNIEnv *env, jobject obj, jobject connect_to, const char *signame)
{
    jobject_cb_info *retval;
    GtkObject *gtko;
    GtkSignalQuery *siginfo;
    guint sigid;
    char *sigsig;

    gtko = JGTKO(obj);
    sigid = gtk_signal_lookup(signame, GTK_OBJECT_TYPE(gtko));
    g_assert(sigid > 0);
    siginfo = gtk_signal_query(sigid);

    sigsig = jobject_signal_signature(siginfo);

    g_free(siginfo);

    retval = g_new(jobject_cb_info, 1);

    retval->obj = (*env)->NewGlobalRef(env, connect_to);
    retval->methodID = (*env)->GetMethodID(env, (*env)->GetObjectClass(env, connect_to), signame, sigsig);
    retval->env = env;

    g_free(sigsig);

    return retval;
}

static void
jobject_cb_info_destroy(gpointer data)
{
  jobject_cb_info *cbi = data;

  (* cbi->env)->DeleteGlobalRef(cbi->env, cbi->obj);
  
  g_free(data);
}

/*
 * Class:     Gtk_Object
 * Method:    signal_connect
 * Signature: (Ljava/lang/String;LGtk/Object;)I
 */
JNIEXPORT jint JNICALL
Java_Gtk_Object_signal_1connect(JNIEnv *env, jobject obj, jstring sigstring, jobject connect_to)
{
    jint retval;
    const char *signame;
    jobject_cb_info *jobject_cb_data;

    signame = (*env)->GetStringUTFChars(env, sigstring, NULL);

    jobject_cb_data = jobject_cb_info_new(env, obj, connect_to, signame);

    retval = gtk_signal_connect_full(JGTKO(obj), signame, NULL, jobject_signal_cb, jobject_cb_data,
				     jobject_cb_info_destroy,
				     FALSE, FALSE);

    (*env)->ReleaseStringUTFChars(env, sigstring, signame);

    return retval;
}

/*
 * Class:     Gtk_Object
 * Method:    signal_disconnect
 * Signature: (I)V
 */
JNIEXPORT void JNICALL
Java_Gtk_Object_signal_1disconnect(JNIEnv *env, jobject obj, jint sigid)
{
}

/*
 * Class:     Gtk_Object
 * Method:    unref
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_Gtk_Object_unref(JNIEnv *env, jobject obj)
{
  gtk_object_unref(JGTKO(obj));
}
