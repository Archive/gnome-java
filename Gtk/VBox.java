package Gtk;

import Gtk.Container;

public class VBox extends Gtk.Box {

	private native void construct(boolean homogeneous, int spacing);

    public VBox(boolean homogeneous, int spacing) {
		construct(homogeneous, spacing);
    }
};
