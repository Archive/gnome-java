#include "Gtk_Container.h"
#include "gtk-java.h"

/*
 * Class:     Gtk_Container
 * Method:    add
 * Signature: (LGtk/Widget;)V
 */
JNIEXPORT void JNICALL
Java_Gtk_Container_add(JNIEnv *env, jobject obj, jobject child)
{
    gtk_container_add(GTK_CONTAINER(jobject_to_gtk(env, obj)),
		      GTK_WIDGET(jobject_to_gtk(env, child)));
}

/*
 * Class:     Gtk_Container
 * Method:    remove
 * Signature: (LGtk/Widget;)V
 */
JNIEXPORT void JNICALL
Java_Gtk_Container_remove(JNIEnv *env, jobject obj, jobject child)
{
    gtk_container_remove(GTK_CONTAINER(jobject_to_gtk(env, obj)),
			 GTK_WIDGET(jobject_to_gtk(env, child)));
}

/*
 * Class:     Gtk_Container
 * Method:    set_border_width
 * Signature: (I)V
 */
JNIEXPORT void JNICALL
Java_Gtk_Container_set_1border_1width(JNIEnv *env, jobject obj, jint width)
{
    gtk_container_set_border_width(GTK_CONTAINER(jobject_to_gtk(env, obj)),
				   width);
}
