#include "Gtk_Window.h"
#include "gtk-java.h"

/*
 * Class:     Gtk_Window
 * Method:    construct
 * Signature: (I)V
 */
JNIEXPORT void JNICALL
Java_Gtk_Window_construct(JNIEnv *env, jobject obj, jint wtype)
{
    jobject_set_gtk(env, obj, GTK_OBJECT(gtk_window_new(wtype)));
}

/*
 * Class:     Gtk_Window
 * Method:    set_title
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_Gtk_Window_set_1title
  (JNIEnv *env, jobject obj, jstring title)
{
	jboolean *copy;

	gtk_window_set_title(GTK_WINDOW(jobject_to_gtk(env, obj)), (*env)->GetStringUTFChars(env, title, copy));
}