package Gtk;

import Gtk.Bin;

public class Window extends Gtk.Bin {
    public static final int TOPLEVEL = 0;
    public static final int DIALOG = 1;
    public static final int POPUP = 2;

    private native void construct(int wtype);

    public Window(int wtype) {
		construct(wtype);
    };

	public native void set_title(String title);
};
