SUBDIRS=Gtk Gnome

all: subdirs test.class

subdirs:
	for I in $(SUBDIRS); do $(MAKE) -C $$I all; done

test.class: test.java
	javac test.java
