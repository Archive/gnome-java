package Gtk;

import Gtk.Container;

public abstract class Box extends Gtk.Container {
  public static final int START = 0;
  public static final int END = 1;

  public native void pack_start (Gtk.Widget child, boolean expand, boolean fill,
                                               int padding);
  public native void pack_end (Gtk.Widget child, boolean expand, boolean fill,
                                               int padding);
  public native void pack_start_defaults (Gtk.Widget widget);
  public native void pack_end_defaults (Gtk.Widget widget);
  public native void set_homogeneous (boolean homogeneous);
  public native void set_spacing (int spacing);
  public native void reorder_child (Gtk.Widget child, int position);
  public native void query_child_packing (Gtk.Widget child, Boolean expand,
                                               Boolean fill,
                                               Integer padding,
                                               Integer pack_type);
  public native void set_child_packing (Gtk.Widget child,
                                               boolean expand,
                                               boolean fill,
                                               int padding,
                                               int pack_type);
};
