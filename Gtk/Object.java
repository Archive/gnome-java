package Gtk;

public class Object {
    int obj_pointer; // XXX We assume we can always fit a native pointer into a Java INT

    public native void ref();
    public native void unref();
    public native java.lang.Object get(String arg_name);
    public native void set_data(String key, java.lang.Object value);
    public native java.lang.Object get_data(String key);
    public native int signal_connect(String name, java.lang.Object cbrecv);
    public native void signal_disconnect(int handler_id);

    static {
	System.loadLibrary("gtk-java");
    };

    public void finalize() {
	unref();
	super.finalize();
    };
};
