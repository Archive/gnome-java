#ifndef GTK_JAVA_H
#define GTK_JAVA_H 1

#include <jni.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>

GtkObject *jobject_to_gtk(JNIEnv *env, jobject obj);
#define JGTKO(x) jobject_to_gtk(env, x)
void jobject_set_gtk(JNIEnv *env, jobject obj, GtkObject *gtkobj);

#endif /* GTK_JAVA_H */
